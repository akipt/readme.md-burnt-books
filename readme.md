# Burnt Books

Burnt Books ist eine Androidanwendung, die eine Liste der am 10. Mai 1933 in Deutschland verbrannten Bücher enthält.

## Funktionen

Die Anwendung zeigt eine Liste der oben genannten Bücher, es können Detailinformationen aus der Deutschen National Bibliothek abrufen, sowie die Bücher zu einer Favoritenliste hinzugefügt werden.

![Bild: Bücherübersicht](img/screen_booklist.png)

![Bild: Buchdetails](img/screen_bookdetail.png)

## Anleitung

Es folgt eine Anleitung, wie das Projekt in Android Studio 2.1.2 zum Laufen gebracht werden kann.

### Vorraussetzungen

Vorraussezungen zum Ausführen der Anwendung in Android Studio sind:

```
Betriebssystem: Linux, OS X oder Windows
[Android Studio](https://developer.android.com/studio/index.html#downloads), 2.1 oder höher, inlusive Android SDK 24
Benutzerkonto bei [Bitbucket](https://bitbucket.org/)
[Git](https://git-scm.com/downloads)
(optional) Mobiltelefon mit Android-Betriebssystem
```

### Installation

Es folgt eine Schritt für Schritt Anleitung

1. In der Konsole den Quellcode herrunterladen via git

```
git clone https://akipt@bitbucket.org/aass16team13/burnt-books.git
```

2. Projekt in Android importieren.

3. Nach dem das Gradlescript fertig geladen ist, das Programm 
a) im Emulator oder b) auf angeschlossenem Telefon starten.

Am Ende sollte das Programm laden und der Startbildschirm sichtbar sein. 

![Bild: Startbildschirm](img/screen_start.png)

## Mitarbeit

Für Anfragen zwecks Verbesserung oder Weiterentwicklung des Projektes, wenden Sie sich bitte an den Autor.

## Autoren

Tom B. [Tomatenbiss](https://bitbucket.org/Tomatenbiss/)

## Lizenz

Verweise auf eine verwendete Lizenz sind nicht vorhanden.
